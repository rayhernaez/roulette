import java.util.Random;

public class RouletteWheel {

    private Random rand;
    private int number; //last spin

    //constructor
    public RouletteWheel() {
        this.rand = new Random();
        this.number = 0;
    }

    //method spin()
    public void spin() {
        this.number = rand.nextInt(37);
    }

    //method getValue()
    public int getValue() {
        return this.number;
    }

    

}