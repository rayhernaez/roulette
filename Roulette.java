import java.util.Scanner;

public class Roulette {
    
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        String msg = reader.nextLine();

        RouletteWheel roulette = new RouletteWheel();
        int bet = 0;
        int amount = 0;

        System.out.println("Do you want to make a bet? Y/N: ");
        if (!"Y".equals(msg)) {
            System.out.println("Try Again!");
          }
        else {
            System.out.println("What number you'd like to bet? 0-36: ");
            bet = reader.nextInt();

            System.out.println("How much are you betting? ");
            amount = reader.nextInt();

            roulette.spin();

            if (roulette.getValue() != this.bet) {
                this.amount = 0;
                System.out.println("You lost.");
            }
            else {
                this.amount = (this.amount) * (roulette.getValue() - 1)
                System.out.println("You win! Now you have " + this.amount ".");
                
            }
        }
    }
}